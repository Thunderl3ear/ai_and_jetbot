# JetBot Mediapipe

## Table of Contents

- [Prerequisites](#prerequisites)
- [Setup](#setup)
- [Details](#details)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Prerequisites
Waveshare JETANK or similar. https://www.waveshare.com/wiki/JETANK_AI_Kit 

* Flash the SD card
* Setup a WiFi connection: `sudo nmcli device wifi connect <SSID> password <PASSWORD>`
* Navigate to `<jetson_ip>:8888`. Password: jetbot
* Start a terminal session in the notebook.
* `git clone https://github.com/waveshare/JETANK.git`
* `cd JETANK`
* `./install.sh` 
* Connect to the host: `ssh jetbot@0.0.0.0`
* `cd JETANK`
* `./config.sh` 
* After the reboot: `ssh jetbot@0.0.0.0`
* `cd jetcard`
* `git pull`
* `./scripts/jetson_install_nvresizefs_service.sh`
* Reboot the Jetson. Now the entire SD card should be available.


## Setup
UPDATE: Mediapipe only works locally, the camera remotely.

Perform all task in the host (`ssh jetbot@0.0.0.0`), not in the notebook Docker.

* Follow the guide found here: https://github.com/feitgemel/Jetson-Nano-Python/tree/master/Install-MediaPipe
* But don't install OpenCV like that. Instead use this script for that: https://github.com/AastaNV/JEP/blob/master/script/install_opencv4.5.0_Jetson.sh 
* Enable the graphical interface: `sudo systemctl set-default graphical.target`
* Next enable graphical SSH. That is, set up XAuth https://superuser.com/questions/806637/xauth-not-creating-xauthority-file
* Ensure `/etc/ssh/sshd_config` has `X11Forwarding yes`
* From windows, use PuTTY with Xming to connect to the jetbot. Ensure the X11 setting is enabled.
* Test the graphical forwarding by running `xclock`

## Details

Strangely, when I then try to run Python with Mediapipe, I get the error `RuntimeError: ; eglGetDisplay() returned error 0x3000ontext_egl.cc:157)`. This error should be related to a missing display, but I've checked that `echo $DISPLAY` exists. Digging a bit deeper into this issue reveals that EGL cannot be rendered remotely over an SSH connection. EGL does not work over remote desktop either.

I've gotten the CSI camera to work on the PuTTY connection.

If I connect a HDMI cable to the Jetson Nano directly and run the Python script in this repository, I can get the following output:

![Demo](mp_demo.png)

To conclude: Installing Mediapipe on the Jetbot is doable, but A LOT of work. Furthermore, the EGL requirement makes it very difficult to use remotely. 

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## Contributers and License
Copyright 2022,

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)


